<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>
<body>
    <h1>Buat Account Baru</h1>

    <h2>Sign Up Form</h2>

    <form action="{{ url('/welcome') }}" method="POST">
        @csrf
        <label for="first name">First name:</label><br>
        <input type="text" name="FirstName" required><br><br>

        <label for="last name">Last name:</label><br>
        <input type="text" name="LastName" required><br><br>

        <label for="last name">Gender:</label><br>
        <input type="radio" id="male" name="gender" value="male" checked>
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>

        <label for="Nationality">Nationality:</label><br>
        <select name="Nationality" id="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>

        <label for="Language Spoken">Language Spoken:</label><br>
        <input type="checkbox" id="Language Spoken" name="Language Spoken" value="Bahasa Indonesia">
        <label for="Bahasa Indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="Language Spoken" name="Language Spoken" value="English">
        <label for="English">English</label><br>
        <input type="checkbox" id="Language Spoken" name="Language Spoken" value="Arabic">
        <label for="Arabic">Arabic</label><br>
        <input type="checkbox" id="Language Spoken" name="Language Spoken" value="Japanese">
        <label for="Japanese">Japanese</label><br><br>
        
        <label for="bio">Bio:</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>

        <button type="submit">Sign Up</button>
    </form>
</body>
</html>