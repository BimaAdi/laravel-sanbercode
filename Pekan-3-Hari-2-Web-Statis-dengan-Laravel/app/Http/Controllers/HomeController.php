<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show index page.
     *
     * @return View
     */
    public function index()
    {
        return view('index');
    }
}
