<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Show register page.
     *
     * @return View
     */
    public function register()
    {
        return view('form');
    }

    /**
     * Show welcome page after register.
     *
     * @param  Request  $request
     * @return View
     */
    public function welcome(Request $request)
    {
        return view('welcome', [
            'FirstName' => $request->input('FirstName'),
            'LastName' => $request->input('LastName')
        ]);
    }

}
