<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $casts = DB::table('cast')->get();

        return view('casts.index', ['casts' => $casts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('casts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);


        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        return redirect('cast')->with('success', 'Cast Berhasil dibuat');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = DB::table('cast')->find($id);

        if ($cast != null) {
            return view('casts.show', ['cast' => $cast]);
        } else {
            return redirect('cast')->with('error', 'cast tidak ditemukan');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('casts.edit', ['cast' => $cast]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $cast = DB::table('cast')
                    ->where('id', $id)
                    ->update([
                        'nama' => $request->input('nama'),
                        'umur' => $request->input('umur'),
                        'bio' => $request->input('bio')
                    ]);
        
        return redirect()
            ->action('CastController@show', ['cast' => $id])
            ->with('success', 'Cast Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = DB::table('cast')->where('id', $id);

        if ($cast != null) {
            $cast->delete();
            return redirect('cast')->with('success', 'Cast Berhasil dihapus');
        } else {
            return redirect('cast')->with('error', 'cast tidak ditemukan');
        }
    }
}
