<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $casts = Cast::all();

        return view('casts.index', ['casts' => $casts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('casts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        Cast::create($request->all());

        return redirect('cast')->with('success', 'Cast Berhasil dibuat');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = Cast::find($id);

        if ($cast != null) {
            return view('casts.show', ['cast' => $cast]);
        } else {
            return redirect('cast')->with('error', 'cast tidak ditemukan');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('casts.edit', ['cast' => $cast]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        
        $cast = Cast::find($id);
        if ($cast != null) {
            $cast->update($request->all());
            return redirect()
                ->action('CastController@show', ['cast' => $id])
                ->with('success', 'Cast Berhasil diupdate');
        } else {
            return redirect('cast')->with('error', 'cast tidak ditemukan');
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = Cast::find($id);

        if ($cast != null) {
            $cast->delete();
            return redirect('cast')->with('success', 'Cast Berhasil dihapus');
        } else {
            return redirect('cast')->with('error', 'cast tidak ditemukan');
        }
    }
}
