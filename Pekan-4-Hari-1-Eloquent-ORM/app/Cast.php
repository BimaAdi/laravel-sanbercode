<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cast';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nama', 'umur', 'bio'];

    /**
     * No created_at and updated_at column.
     *
     * @var bool
     */
    public $timestamps = false;
}
