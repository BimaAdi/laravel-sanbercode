@extends('layouts.app')

@section('title', 'Cast')

@section('content')
<div class="card card-secondary">

    <div class="card-header with-border">
        <div class="card-title">Edit Cast</div>
    </div>

    <form method="POST" action="{{ url('cast/'.$cast->id) }}}">
        @method('PUT')
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" placeholder="Masukan nama" name="nama" value="{{ old('nama', $cast->nama) }}">
                
                @error('nama')
                    <!-- error message nama -->
                    <span id="exampleInputEmail1-error" class="error invalid-feedback d-block">{{ $message }}</span>
                @enderror

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Umur</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan umur" name="umur" value="{{ old('umur', $cast->umur) }}">

                @error('umur')
                    <!-- error message umur-->
                    <span id="exampleInputEmail1-error" class="error invalid-feedback d-block">{{ $message }}</span>
                @enderror
                
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Bio</label>
                <textarea class="form-control" rows="3" placeholder="Enter ..." name="bio">{{ old('bio', $cast->bio) }}</textarea>
                
                @error('bio')
                    <!-- error message bio-->
                    <span id="exampleInputEmail1-error" class="error invalid-feedback d-block">{{ $message }}</span>
                @enderror

            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-success" style="width: 80px">Edit</button>
            <a href="javascript:history.back()" class="btn btn-secondary" style="width: 80px">Batal</a>
        </div>
    </form>
    
</div>
@endsection
