@extends('layouts.app')

@section('title', 'Cast')

@section('content')

<!-- success message -->
@if(session('success'))
    <div class="alert alert-success alert-dismissible mt-2 fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

<div class="card card-secondary">


    <div class="card-header with-border">
        <div class="card-title">Show Cast</div>
    </div>

    <div class="card-body">
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" placeholder="Masukan nama" name="nama" value="{{ $cast->nama }}" disabled>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Umur</label>
            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan umur" name="umur" value="{{ $cast->umur }}" disabled>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Bio</label>
            <textarea class="form-control" rows="3" placeholder="Enter ..." name="bio" disabled>{{ $cast->bio }}</textarea>
        </div>
    </div>

    <div style="display: flex">
        <div class="card-footer">
            <a class="btn btn-success" style="width: 80px" href="{{ url('cast/'.$cast->id.'/edit') }}">Edit</a>
        </div>
        <form method="POST" action="{{ url('cast/'.$cast->id) }}}">
            @method('DELETE')
            @csrf
            <div class="card-footer">
                <button type="submit" style="width: 80px; margin-left:-38px" class="btn btn-danger">Hapus</button>
            </div>
        </form>
        <div class="card-footer">
            <a href="{{ url('cast/')}}" class="btn btn-secondary" style="width: 90px; margin-left:-38px">All Cast</a>
        </div>
    </div>
    
</div>
@endsection
