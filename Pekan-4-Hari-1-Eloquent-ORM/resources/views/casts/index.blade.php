@extends('layouts.app')

@section('title', 'Cast')

@section('content')

<!-- success message -->
@if(session('success'))
<div class="alert alert-success alert-dismissible mt-2 fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="card">
    <div class="card-header">
      <h3 class="card-title">All Cast</h3>
    </div>

    <!-- Tabel -->
    <div class="card-body">
        
        <!-- create button -->
        <a class="btn btn-block btn-sm btn-primary w-25" href="{{ url('cast/create') }}">Buat Cast baru</a>

        <!-- error message -->
        @if(session('error'))
            <div class="alert alert-danger alert-dismissible mt-2 fade show" role="alert">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        
        <!-- table cast -->
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

                @foreach ($casts as $cast)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $cast->nama }}</td>
                    <td>
                        <a class="btn btn-block btn-sm btn-success" href="{{ url('cast/'.$cast->id.'/') }}">Detail</a>
                    </td>
                </tr>
                @endforeach
                
            </tbody>
            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>

    </div>

@endsection

@push('extra-scripts')
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush