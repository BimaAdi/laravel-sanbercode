<?php
function tukar_besar_kecil($string){
    // string to array char
    $stringArray = str_split($string, 1);

    // String ke ASCII
    $ascii = [];
    foreach ($stringArray as $item) {
        $ascii[] = ord($item);
    }

    // Tukar huruf besar dengan huruf kecil dan sebaliknya
    // jika nilai ascii antara 97 dan 122 berarti huruf kecil, 
    // jika nilai ascii antara 65 dan 90 berarti huruf besar
    // selain itu bukan huruf
    $tukarAscii = [];
    foreach ($ascii as $item) {
        if ($item >= 97 && $item <=122) {
            $tukarAscii[] = $item - 32;
        } else if ($item >= 65 && $item <= 90){
            $tukarAscii[] = $item + 32;
        } else {
            $tukarAscii[] = $item;
        }
    }

    // ASCII to char
    $AsciiToChar = [];
    foreach ($tukarAscii as $item) {
        $AsciiToChar[] = chr($item);
    }

    return implode($AsciiToChar)."<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>