<?php
function ubah_huruf($string){
    // string to array char
    $stringArray = str_split($string, 1);

    // String ke ASCII
    $ascii = [];
    foreach ($stringArray as $item) {
        $ascii[] = ord($item);
    }

    // ASCII + 1
    $asciiPlusSatu = [];
    foreach ($ascii as $item) {
        $asciiPlusSatu[] = $item + 1;
    }

    // ASCII to char
    $AsciiToChar = [];
    foreach ($asciiPlusSatu as $item) {
        $AsciiToChar[] = chr($item);
    }

    return implode($AsciiToChar)."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>